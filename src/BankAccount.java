
public class BankAccount {
	private int accNo;
	private double balance;
	private String customerName;
	private String email;
	private int phoneNumber;
	
	public BankAccount() {
		this(56789,2.5,"Default name","Default emailAddress",123456789); // "this" keyword calls the the main constructor
		// which is below. this has to be the first link in the constructor
		System.out.println("Empty constructor called");
	}
	
	public BankAccount(int accNo, double balance, String customerName, String email, int phoneNumber) {
		System.out.println("Account constructor with parameters called");
		this.accNo = accNo;
		this.balance = balance;
		this.customerName = customerName;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}
	
	// Another used case of "this" keyword, as only 3 parameters are passed in the constructor, and the remaining 2 is initialized by us
	public BankAccount(String customerName, String email, int phoneNumber) {
		this(10239,100.55,customerName,email,phoneNumber);
		this.customerName = customerName;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}

	public void depositFunds(double depositAmount) {
		this.balance=this.balance+depositAmount;
		System.out.println("Deposit of "+depositAmount+" processed. New balance is "+this.balance);
	}
	
	public void withdrawFunds(double withdrawalAmount) {
		if(withdrawalAmount>this.balance) {
			System.out.println("Only "+this.balance+" is available. Withdrawal not successful");
		}
		else
			this.balance=this.balance-withdrawalAmount;
			System.out.println("Withdrawal of "+withdrawalAmount+" processed. Remaining balance is "+this.balance);
	}
	

	public int getAccNo() {
		return accNo;
	}
	public void setAccNo(int accNo) {
		this.accNo = accNo;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
