
public class Main {

	public static void main(String[] args) {
		Car porsche = new Car();
		Car holden= new Car();
//		porsche.model="Carrera";  This is possible only when String model is public
		porsche.setModel("Carrera"); // This is required when String model is private
		System.out.println("Model is "+porsche.getModel());
		
		BankAccount bobsAccount=new BankAccount(12345,0,"Bob","myemail@bob.com",9832903);// This initialization is done with the help of constructor
		BankAccount alanAccount=new BankAccount(); // Demonstration of "this" keyword. First the account constructor is called, then the empty constructor is called.
		
		/*
		BankAccount bobsAccount = new BankAccount();
		bobsAccount.setAccNo(12345);
		bobsAccount.setBalance(0);
		bobsAccount.setCustomerName("Bob");
		bobsAccount.setEmail("myemail@bob.com");
		bobsAccount.withdrawFunds(100.0);
		*/ // This code of using setters to initialize will be only required when Constructor is not created.
		
		bobsAccount.depositFunds(50.0);
		bobsAccount.withdrawFunds(100.0);
		
		bobsAccount.depositFunds(51.0);
		bobsAccount.withdrawFunds(100.0);
		
		VipCustomer customer1=new VipCustomer();
		System.out.println(customer1.getName());
		
		VipCustomer customer2=new VipCustomer("Bob",25000.00);
		System.out.println(customer2.getName());
		
		VipCustomer customer3=new VipCustomer("Tim",100.00,"tim@email.com");
		System.out.println(customer3.getName()+" "+customer3.getEmailID());
		
		
	}

}
