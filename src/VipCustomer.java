
public class VipCustomer {
	private String name;
	private double credit_limit;
	private String emailID;
	public VipCustomer() {
		this("Default name",9999,"Default emailID");
	}
	public VipCustomer(String name,double credit_limit) {
		this(name,credit_limit,"default@emailid.com");
		this.name=name;
		this.credit_limit = credit_limit;
	}
	
	public VipCustomer(String name, double credit_limit, String emailID) {
		this.name = name;
		this.credit_limit = credit_limit;
		this.emailID = emailID;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getCredit_limit() {
		return credit_limit;
	}

	public void setCredit_limit(int credit_limit) {
		this.credit_limit = credit_limit;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	
}
